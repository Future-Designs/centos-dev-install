#!/usr/bin/env bash

// epel and webtatic
sudo yum -y update
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
sudo yum -y update

// php
sudo yum -y install mod_php71w php71w-opcache php71w-cli php71w-common php71w-gd php71w-intl php71w-mbstring php71w-mcrypt php71w-mysql php71w-mssql php71w-pdo php71w-pear php71w-soap php71w-xml php71w-xmlrpc

// composer
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

// docker
sudo yum -y install docker

// docker-compose
// https://github.com/docker/compose/releases
// look for newest version
sudo curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose

// git
sudo yum -y install git

// ssh keys
//cat ~/.ssh/id_rsa.pub
sudo ssh-keygen -t rsa

// mariadb

sudo yum install mariadb-server mariadb-client

